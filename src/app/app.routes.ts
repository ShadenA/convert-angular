
import { RouterModule, Routes } from '@angular/router';
import { ConvertComponent } from './components/pages/convert/convert.component';
import { LoadComponent } from './components/pages/load/load.component';
import { DownloadsComponent } from './components/pages/downloads/downloads.component';
import { AboutUsComponent } from './components/pages/about-us/about-us.component';

const APP_ROUTES: Routes = [
    { path: 'convert', component: ConvertComponent },
    { path: 'load/:name', component: LoadComponent },
    { path: 'downloads', component: DownloadsComponent },
    { path: 'about', component: AboutUsComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'convert' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, { useHash: true });
