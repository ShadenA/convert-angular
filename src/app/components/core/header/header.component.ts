import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../../services/service.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {


  constructor( public servicio: ServiceService) {}

  ngOnInit() {
  }

}
