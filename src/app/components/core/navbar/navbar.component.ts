import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../../../services/service.service';

export class Menu {
  name: string;
}

const menus: Menu[] = [
  { name: 'Convertidor' },
  { name: 'Descargas' },
  { name: 'Sobre Nosotros' }
];

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {


  constructor( private router: Router,
               private servicio: ServiceService ) { }

  isMenuCollapsed = false;

  menu = menus;
  selectedMenu = { name: 'Convertidor' };
  onselect(M: Menu ): void {
    this.selectedMenu = M;
    if ( this.selectedMenu.name === 'Convertidor' ) {
      this.router.navigate(['/convert']);
      this.servicio.titulo = 'Convertidor';
    }
    if (this.selectedMenu.name === 'Descargas' ) {
      this.router.navigate(['/downloads']);
      this.servicio.titulo = 'Descargas';
    }
    if (this.selectedMenu.name === 'Sobre Nosotros' ) {
      this.router.navigate(['/about']);
      this.servicio.titulo = 'Sobre Nosotros';
    }
  }
  onload = () => {
    return this.onselect;
  }

  ngOnInit() {
  }

}
