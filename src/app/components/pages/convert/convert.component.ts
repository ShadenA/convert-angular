import { Component, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ServiceService } from 'src/app/services/service.service';

@Component({
  selector: 'app-convert',
  templateUrl: './convert.component.html',
  styleUrls: ['./convert.component.css']
})
export class ConvertComponent implements AfterViewInit {

  @ViewChild('modal', {static: false }) Modal: ElementRef;
  comprobando: boolean;
  fileName: string;
  public archivoenviado;
  constructor( private router: Router,
               private modalService: NgbModal,
               private servicio: ServiceService ) {
               }

  ngAfterViewInit() {
    if ( this.servicio.mod ) {
      this.openModal(this.Modal);
      this.servicio.mod = false;
    }
  }

  openModal(modal) {
    this.modalService.open(modal);
  }
  cerrarModal() {
    this.modalService.dismissAll();
  }

  fileuploads( event ) {
    const file = event.target.files;
    this.servicio.getprocesarArchivo(file);
    this.fileName = file[0].name;
    this.comprobando = true;
    if (this.comprobando) {
      return setTimeout(() => {
        this.router.navigate(['/load', this.fileName]);
      }, 2000);
    }

  }



}
