import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// import { NgbProgressbarB } from '@ng-bootstrap/ng-bootstrap';
import { ServiceService } from '../../../services/service.service';


@Component({
  selector: 'app-load',
  templateUrl: './load.component.html',
  styleUrls: ['./load.component.css']
})
export class LoadComponent implements OnInit {

  name: string;
  progressNumber: number;
  ayuda: any;
  ruta: {
    file;
  };

  constructor( private route: ActivatedRoute,
               private router: Router,
               private servicio: ServiceService ) {
  }

  ngOnInit() {
    const name = this.route.snapshot.paramMap.get('name');
    this.name = name;
    this.progressNumber = 10;
    const barra = setInterval( () => {
      this.progressNumber ++;
      if (this.progressNumber === 100) {
        clearInterval(barra);
        // tslint:disable-next-line:no-unused-expression
        this.servicio.archivotxt;
        this.ruta = this.servicio.archivotxt;
        console.log(this.ruta);
        }
    } , 150);

  }

  confirmation() {
    if (confirm('Seguro que desea cancelar la conversión?')) {
      this.router.navigate(['/convert']);
    } else {
      return false;
    }
  }


}
