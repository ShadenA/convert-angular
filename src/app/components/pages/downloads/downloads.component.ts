import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/services/service.service';


@Component({
  selector: 'app-downloads',
  templateUrl: './downloads.component.html',
  styleUrls: ['./downloads.component.css']
})
export class DownloadsComponent implements OnInit {

  descargas: [];

  constructor( private servicio: ServiceService ) { }

  ngOnInit() {
    this.servicio.getDescargas();
    this.descargas = this.servicio.descargados;
  }

}
