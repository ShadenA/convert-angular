// modulos
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
// componentes
import { FooterComponent } from 'src/app/components/core/footer/footer.component';
import { HeaderComponent } from 'src/app/components/core/header/header.component';
import { NavbarComponent } from 'src/app/components/core/navbar/navbar.component';
import { AboutUsComponent } from 'src/app/components/pages/about-us/about-us.component';
import { DownloadsComponent } from 'src/app/components/pages/downloads/downloads.component';
import { ConvertComponent } from 'src/app/components/pages/convert/convert.component';
import { LoadComponent } from 'src/app/components/pages/load/load.component';

// servicio
import { ServiceService } from '../../services/service.service';


@NgModule({
  declarations: [
    ConvertComponent,
    LoadComponent,
    DownloadsComponent,
    AboutUsComponent,
    HeaderComponent,
    FooterComponent,
    NavbarComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    HttpClientModule
  ],
  exports: [
    ConvertComponent,
    LoadComponent,
    DownloadsComponent,
    AboutUsComponent,
    HeaderComponent,
    NavbarComponent,
    FooterComponent,
    NgbModule
  ],
  providers: [
    ServiceService
  ]
})
export class ComponentsModule { }
