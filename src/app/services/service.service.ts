import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  mod = true;
  titulo = 'Convertidor';
  ayuda = new HttpHeaders({'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'});
  archivo: any;
  archivotxt;
  descargados;
  url2 = 'http://127.0.0.1:8000/api/';

  constructor( private http: HttpClient ) {
  }

  getprocesarArchivo(  file: FileList ) {

    // conversion de tipo filelist a file
    // tslint:disable-next-line:forin
    for ( const propiedad in Object.getOwnPropertyNames( file )) {
      const archivoNuevo = file[propiedad];
      this.archivo = archivoNuevo;
    }

    // interface Archivourl { url: string; }

    // haciendo peticion tipo post
    const formData =  new FormData();
    formData.append('archivo', this.archivo);
    return this.http.post<any>(`${ this.url2 }archivo/`, formData)
    .subscribe(response => {
      this.archivotxt = response;
      console.log(this.archivotxt);
    });
  }

  getDescargas( ) {
    return this.http.get(`${ this.url2 }archivo/list/`, { headers: this.ayuda }).toPromise().then(
      resp => {
        console.log(resp);
        this.descargados = resp;
      }
    );
  }
}
